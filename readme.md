# Premier site en PHP

## Structure du projet

Pour empêcher les internautes d'accéder à tous les fichiers du projet, _(ex fichiers sensibles, `git ignore`, templates)_, il est nécessaire de définir une structure de dossier solide, et de configurer correctement le serveur web **APACHE**.

Le dossier `src` : contient tous le **code source** du projet(fichier php uniquement).

Le dossier `public` : contient toutes les ressources accessibles depuis le web. **Chaque fichiers qui se trouve dans le dossier public possède une URL**

