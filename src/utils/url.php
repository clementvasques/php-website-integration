<?php

// site.com/contact.php => contact.php
// localhost/mon-dossier/contact.php => contact.php
// localhost/mon-dossier/sous-dossier/contact.php => sous-dossier/contact.php
// localhost/mon-dossier/sous-dossier/contact.php => sous-dossier/contact.php
// localhost/d1/d2/d3/contact.php => d2/d3/contact.php

// localhost/contact.php?name=riri => contact.php
// localhost/contact.php?name=riri&age=18 => contact.php
// localhost/contact.php#services => contact.php
// localhost/contact.php?name=riri#services => contact.php

// site.com => ""
// site.com/ => ""

// localhost/mon-dossier => ""
// localhost/mon-dossier/ => ""

// localhost/mon-dossier/sous-dossier => "sous-dossier"
// localhost:8080/mon-dossier/sous-dossier => "sous-dossier"


function getUrlPath () {
    // on part d'abord de l'URL complète
    $urlPath = $_SERVER['REQUEST_URI'];

    // on enlève le dossier "racine" qui contient le site
    $urlPath = str_replace(ROOT_FOLDER, '', $urlPath);

    $urlPath = str_replace($_SERVER['QUERY_STRING'], '', $urlPath);
    $urlPath = trim($urlPath, '?');
    return $urlPath;
};
