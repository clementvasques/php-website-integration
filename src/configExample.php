<?php

// constante PHP qui représente l'URL absolue racine du site
define ("BASE_URL", "mettez votre URL racine ici");

// config d'exemple;
define ("ROOT_FOLDER", "le nom du dossier du site sur le serveur web");


// config pour la base de données
define ("DB_HOST", "localhost:3306");
define ("DB_NAME", "nameDataBase");
define ("DB_USER", "user");
define ("DB_PASSWORD", "***");