<div class="footer">
    <div class="containerIcons">
        <div class="icon1"></div>
        <div class="icon2"></div>
        <div class="icon3"></div>
    </div>
    <div class="contact">Contact us</div>
    <br>
    <div class="email">info@email.com</div>
    <br>
    <div class="copyright">Copyright &#169 2018 All rights reserved | This templates is made with <span>&#9829</span> by Colorlib</div>
</div>