<?php

require_once __DIR__ . '/../utils/url.php';

$currentUrl = getUrlPath();

$menuLinks = [
    [
        "url" => "index.php",
        "label" => "Home",
    ],
    [
        "url"=>"project-single.php",
        "label"=> "All projects"
    ],
    [
        "url" => "aboutus.php",
        "label" => "About us",
    ],
    [
        "url" => "services.php",
        "label" => "Services",
    ],
    [
        "url" => "contact.php",
        "label" => "Contact",
    ]
]

?>

<div class="containerMenu">
    <div class="menu">

        <ul>
            <p>[TOUGH]</p>
            <br>
            <?php foreach ($menuLinks as $link): ?>
            <li>
                <a
                        href="<?= $link['url'] ?>"
                        class="menu-link <?= $currentUrl === $link['url'] ? "selected": "" ?>"
                >
                    <?= $link['label'] ?>
                </a>
            </li>
            <?php endforeach; ?>

        </ul>
    </div>
</div>