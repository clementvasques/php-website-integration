<?php $work = "work" ?>

<section id="section4">
    <div class="column">
        <div class="portfolio">
            <h2>Portfolio</h2>
            <br>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
        </div>
        <div class="containerimg">
            <div class="firstImage">
                <div class="filterImage">
                </div>
            </div>
            <div class="descriptionImage">Work></div>
        </div>
        <div class="containerimg">
            <div class="classicImage1">
                <div class="filterImage">
                </div>
            </div>
            <div class="descriptionImage">Work</div>
        </div>
        <div class="containerimg">
            <div class="classicImage2">
                <div class="filterImage">
                </div>
            </div>
            <div class="descriptionImage">Work</div>
        </div>
    </div>
    <div class="containerLargeImage">
        <div class="containerimg">
            <div class="largeImage">
                <div class="filterImage">
                </div>
            </div>
            <div class="descriptionImage">Work</div>
        </div>
        <div class="containerimg">
            <div class="containerClassic">
                <div class="classicImage3">
                    <div class="filterImage">
                    </div>
                </div>
                <div class="descriptionImage">Work</div>
                <div class="containerimg2"></div>

                <div class="classicImage4">
                    <div class="filterImage">
                    </div>
                </div>
                <div class="descriptionImage4">Work</div>
            </div>
        </div>
        <div class="containerimg">
            <div class="containerLast">
                <div class="mediumImage">
                    <div class="filterImage">
                    </div>
                </div>
                <div class="descriptionImage">Work</div>
                <div class="containerimg2"></div>
                <div class="lastImage">
                    <div class="filterImage">
                    </div>
                </div>
                <div class="descriptionImageLast">Work</div>
            </div>

        </div>

    </div>
</section>