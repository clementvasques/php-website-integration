<?php

$title="Contact";

//include  __DIR__ . '/forms/traitement-contact.php';

$dataReceived = false;
$error = [];
$validValues = [];

if ($_SERVER['REQUEST_METHOD'] === 'POST' && !empty($_POST)){
    $dataReceived = true;


    if (array_key_exists('prenom', $_POST)) {
        // On vérifie qu'il y a une valeur associée a cet index
        if (empty($_POST['prenom'])) {
            // Erreur !!
            $error['prenom'] = "Le prénom est obligatoire.";
        } else {
            $validValues['prenom'] = $_POST['prenom'];
        }
    }

    if (array_key_exists('nom', $_POST)) {
        // On vérifie qu'il y a une valeur associée a cet index
        if (empty($_POST['nom'])) {

        } else {
            $validValues['nom'] = $_POST['nom'];
        }
    }

    if (array_key_exists('adresse', $_POST)) {
        if (!empty($_POST['adresse'])) {
            $validValues['adresse'] = $_POST['adresse'];
        }
    }

    if (array_key_exists('rating', $_POST)) {
        if (!empty($_POST['rating'])) {
            $validValues['rating'] = $_POST['rating'];
        }
    }

    if (array_key_exists('metier', $_POST)) {
        if (!empty($_POST['metier'])) {
            $validValues['metier'] = $_POST['metier'];
        }
    }

    if (array_key_exists('telephone', $_POST)) {
        if (empty($_POST['telephone'])) {

        } else if (!ctype_digit($_POST['telephone'])) {
            $error['telephone'] = "Que des nombres please";
        } else {
            $validValues['telephone'] = $_POST['telephone'];

        }
    }

    if (array_key_exists('email', $_POST)) {
        // On vérifie qu'il y a une valeur associée a cet index
        if (empty($_POST['email'])) {
            // Erreur !!
            $error['email'] = "Veuillez entrer votre mail pour continuer";
        } else {
            $validValues['email'] = $_POST['email'];
        }
    }

    //TODO : bug sur l'affichage de valid values lorsque le formulaire général n'est pas valide, on est obligé de retaper son message
    if (array_key_exists('textzone', $_POST)) {
        if (empty($_POST['textzone'])) {
            $error['textzone'] = "Le message est obligatoire.";
        } else if (strlen($_POST['textzone']) < 5 || str_word_count($_POST['textzone']) < 3) {
            $error['textzone'] = "Merci de développer un peu. (5 lettres, 3 mots minimum !)";
        } else if (strlen($_POST['message']) > 280) {
            $error['textzone'] = "Merci de synthétiser un peu ! (280 caractères maximum)";
        } else {
            $validValues['textzone'] = $_POST['textzone'];
        }
    }

    if (!empty($_POST)){
        // Trouve le fichier de config pour la BDD
        require_once __DIR__ . "/../src/config.php";

        // Connexion à la base de donnée
        $dataBaseConnection = new PDO('mysql:host=' . DB_HOST. ':3306;dbname='. DB_NAME .';charset=utf8',DB_USER,DB_PASSWORD);


        $feedbacksForm = $dataBaseConnection
            ->query("
        INSERT INTO feedbacks (nom, prenom, email, telephone, rating, textzone, metier, adresse)
        VALUES ('". addcslashes($_POST['nom'], "'") ."', '". addcslashes($_POST['prenom'], "'") ."', '". addcslashes($_POST['email'], "'") ."', '". addcslashes($_POST['telephone'], "'") ."', '". addcslashes($_POST['rating'], "'") ."', '". addcslashes($_POST['textzone'], "'") ."', '". addcslashes($_POST['metier'], "'") ."', '". addcslashes($_POST['adresse'], "'") ."')
        ");

    }
}

if (empty($error)){
    $validValues = [];
}

//var_dump($dataBaseConnection->errorInfo());
//exit;

?>

<!doctype html>
<html lang="fr">
    <?php include '../src/templates/head.php'?>

<body>
    <?php include '../src/templates/menu.php' ?>

    <?php include '../src/templates/hamburger.php' ?>


    <br>
    <br>
    <br>
    <div class="globalContainerContact">
        <h1>Contact</h1>
<!--        --><?php //var_dump($_POST) ?>
<!--        --><?php //var_dump($error) ?>
<!--        --><?php //var_dump($dataReceived) ?>
<!--        --><?php //var_dump($validValues) ?>




        <form action="contact.php" method="post" class="form">
            <div>
                <label for="prenom">Prénom : <span>*</span></label>
                <input type="text" id="prenom" name="prenom" value="<?= !empty($validValues['prenom']) ? $validValues['prenom'] : ''?>">
<!--                --><?php //var_dump($error);
//                exit;
//                ?>
                <?php if (isset($error['prenom'])): ?>
                    <div class="error">
                        <?= $error['prenom'] ?>
                    </div>
                    <?php else: ?>
                    <div class="error">
                    </div>
                <?php endif ?>
            </div>
            <div>
                <label for="nom">Nom : </label>
                <input type="text" id="nom" name="nom" value="<?= !empty($validValues['nom']) ? $validValues['nom'] : ''?>">
                <?php if (isset($error['nom'])): ?>
                    <div class="error">
                        <?= $error['nom'] ?>
                    </div>
                <?php else: ?>
                    <div class="error">
                    </div>
                <?php endif ?>
            </div>
            <div>
                <label for="email">Email :  <span>*</span></label>
                <input type="email" id="email" name="email" value="<?= !empty($validValues['email']) ? $validValues['email'] : ''?>">
                <?php if (isset($error['email'])): ?>
                    <div class="error">
                        <?= $error['email'] ?>
                    </div>
                <?php else: ?>
                    <div class="error">
                    </div>
                <?php endif ?>
            </div>
            <div>
                <label for="telephone">Téléphone : </label>
                <input type="phone" id="telephone" name="telephone" value="<?= !empty($validValues['telephone']) ? $validValues['telephone'] : ''?>">
                <?php if (isset($error['telephone'])): ?>
                    <div class="error">
                        <?= $error['telephone'] ?>
                    </div>
                <?php else: ?>
                    <div class="error">
                    </div>
                <?php endif ?>
            </div>
            <div>
                <label for="adresse">Adresse : </label>
                <input type="text" id="adresse" name="adresse" value="<?= !empty($validValues['adresse']) ? $validValues['adresse'] : ''?>">
                <?php if (isset($error['adresse'])): ?>
                    <div class="error">
                        <?= $error['adresse'] ?>
                    </div>
                <?php else: ?>
                    <div class="error">
                    </div>
                <?php endif ?>
            </div>
            <div>
                <label for="metier">Métier</label>
                <input type="text" id="metier" name="metier" value="<?= !empty($validValues['metier']) ? $validValues['metier'] : ''?>">
                <?php if (isset($error['metier'])): ?>
                    <div class="error">
                        <?= $error['metier'] ?>
                    </div>
                <?php else: ?>
                    <div class="error">
                    </div>
                <?php endif ?>
            </div>
            <div>
                <label for="textzone">Ecrivez votre commentaire ici ! <span>*</span></label>
                <textarea id="textzone" name="textzone" value="<?= !empty($validValues['textzone']) ? $validValues['textzone'] : ''?>"></textarea>
                <?php if (isset($error['textzone'])): ?>
                    <div class="error">
                        <?= $error['textzone'] ?>
                    </div>
                <?php else: ?>
                    <div class="error">
                    </div>
                <?php endif ?>
            </div>
            <div>
                <label for="rating">Mettez une note</label>
                <input type="range" step="1" id="rating" name="rating" min="0" max="5" value="<?= !empty($validValues['rating']) ? $validValues['rating'] : ''?>">
                <?php if (isset($error['rating'])): ?>
                    <div class="error">
                        <?= $error['rating'] ?>
                    </div>
                <?php else: ?>
                    <div class="error">
                    </div>
                <?php endif ?>
            </div>

            <button class="formButton" type="submit">Envoyer</button>
        </form>
        <div class="validationMessage">
            <?php if ($dataReceived): ?>
                <?php if (!empty($error)): ?>
                    <div class="box boxError">Le formulaire contient des erreurs :(</div>
                <?php endif ?>
                <?php if(empty($error)): ?>
                    <div class="box boxValide">Le formulaire a bien été envoyé !</div>
                <?php endif ?>
            <?php endif ?>
        </div>

    </div>

    <?php include '../src/templates/footer.php' ?>

    <script src="./menu.js"></script>

</body>
</html>