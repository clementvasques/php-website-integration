<?php

session_start();

if (empty($_SESSION['connected'])){
    header("Location: login.php");
    exit;
}

require_once __DIR__ . "/../../src/config.php";

$dataBaseConnection = new PDO('mysql:host=' . DB_HOST. ':3306;dbname='. DB_NAME .';charset=utf8',DB_USER,DB_PASSWORD);

$servicesTable = $dataBaseConnection ->query("SELECT * FROM services");


$title = "Update Article | Admin";

if ($servicesTable !== false){
    $servicesOk = $servicesTable->fetchAll(PDO::FETCH_ASSOC);
    $donnees = $servicesTable;
    var_dump($servicesOk);
} else {
    $error = $dataBaseConnection->errorInfo();
    $errorMessage = $error[2];
}



if (!empty($_GET['id'])){
    var_dump($_GET['id']);

    $serviceOk['id'] = $_GET['id'];
    var_dump($serviceOk['id']);
}
$serviceOk['id'] = $_GET['id'];
var_dump($_GET['name']);
?>


<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $title ?></title>

    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
          integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l"
          crossorigin="anonymous">
</head>
<body>

<div class="container-sm">
    <br>
    <br>
    <h1 class="nameArticle mb-3">Modifier l'article <?= $_GET['name']?></h1>
    <form>
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" placeholder="<?= $_GET['name']?>">
        </div>
        <div class="form-group">
            <label for="textzone">Commentaire</label>
            <input class="form-control" id="textzone" rows="3" value="<?= $_GET['name']?>"></input>
        </div>
        <div class="form-group">
            <label for="image">Image</label>
            <input type="file" class="form-control" id="image">
        </div>
    </form>
</div>


</body>
</html>
