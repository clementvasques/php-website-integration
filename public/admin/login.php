<?php
require_once __DIR__ . "/../../src/config.php";


$dataBaseConnection = new PDO('mysql:host=' . DB_HOST. ':3306;dbname='. DB_NAME .';charset=utf8',DB_USER,DB_PASSWORD);

$databaseLog = $dataBaseConnection->query("SELECT * FROM login");

//var_dump($databaseLog);

if ($databaseLog !== false) {
    $databaseLogOk = $databaseLog->fetchAll(PDO::FETCH_ASSOC);
}

session_start();

$_SESSION['connected'] = false;

foreach ($databaseLogOk as $data){
    if (empty($_POST)){
        // on fait rien
    } else if ($_POST['identifiant'] == $data['login']){
//        echo 'yoyoyo';
//        exit;
        if (password_verify($_POST['password'], $data['password'])){
            $_SESSION['connected'] = true;
            header("Location: admin.php");
            exit;
        } else {
            $errorPassword = "erreur mp";
        }
    } else {
        $errorID = "Votre identifiant ou mp est erroné";
    }
}

?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login administration</title>

    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
          integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l"
          crossorigin="anonymous">
</head>
<body>
    <div class="container-sm">
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <h1>Login</h1>
        <br>
        <br>
        <form method="post">
            <div class="mb-3">
                <label for="identifiant" class="form-label">Identifiant</label>
                <input type="identifiant" class="form-control" id="identifiant" name="identifiant">
                <?php if (!empty($errorID)): ?>
                    <div class="error alert alert-danger mt-3">Votre id est erroné</div>
                <?php endif; ?>

            </div>
            <div class="mb-3">
                <label for="password" class="form-label">Mot de passe</label>
                <input type="password" class="form-control" id="password" name="password" >
                <?php if (!empty($errorPassword)): ?>
                    <div class="error alert alert-danger mt-3">Votre mp est erroné</div>
                <?php endif; ?>
            </div>
            <button type="submit"  class="btn btn-primary">Valider</button>
        </form>
    </div>

</body>
</html>
