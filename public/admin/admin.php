<?php
session_start();

require_once __DIR__ . "/../../src/config.php";


if (empty($_SESSION['connected'])){
    header("Location: login.php");
    exit;
}

$dataBaseConnection = new PDO('mysql:host=' . DB_HOST. ':3306;dbname='. DB_NAME .';charset=utf8',DB_USER,DB_PASSWORD);

$servicesTable = $dataBaseConnection ->query("SELECT * FROM services");

if ($servicesTable !== false){
    $servicesOk = $servicesTable->fetchAll(PDO::FETCH_ASSOC);
    $donnees = $servicesTable;
//    var_dump($servicesOk);
} else {
    $error = $dataBaseConnection->errorInfo();
    $errorMessage = $error[2];
}

if (array_key_exists('id', $_GET)){
    if (!empty($_GET['id'])){
//        $deleteLineTable = $dataBaseConnection ->query("DELETE FROM services WHERE id=5");
    }
}

if (!empty($_GET['service'])) {
    // Le paramètre est bien passé, on doit regarder s'il est valide

    // Tout ce qui vient de $_GET est de type "string", donc on convertit en "integer"
    $service['id'] = intval($_GET['service']);

}

?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>

    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
          integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l"
          crossorigin="anonymous">
    <link rel="stylesheet" href="/public/style.css">

</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">[TOUGH]</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-link active" aria-current="page" href="#">Home</a>
                    <a class="nav-link" href="#">Services</a>
                    <a class="nav-link" href="#">Feedbacks</a>
                </div>
            </div>
        </div>
    </nav>

    <div class="container-sm">
        <br>
        <br>
        <br>
        <h1 class="display-3">Services</h1>
        <br>
        <br>

        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Article name</th>
                <th scope="col">Description</th>
                <th scope="col">Image</th>
                <th scope="col"></th>
                <th scope="col"></th>

            </tr>
            </thead>
            <tbody>
            <?php
//            if (!empty($_POST)){
//                $getID = $_POST['supprimer'];
//                var_dump($getID);
//                $deleteLineTable = $dataBaseConnection ->query("DELETE FROM services WHERE id='$getID'");
//                var_dump($deleteLineTable);
//            }

//            if (!empty($_GET)){
//                $get = $_GET['id'];
//                var_dump($get);
//                $deleteLineTable = $dataBaseConnection ->query("DELETE FROM services WHERE id='$get'");
//                var_dump($deleteLineTable);
//            }

            ?>

        <?php $i = 0; ?>
        <?php foreach ($servicesOk as $service): ?>
            <?php $i++ ?>
            <tr>
                <th scope="row"><?= $i ?></th>


                    <?php if (isset($service['name'])): ?>
                    <td><?= $service['name']?></td>
                    <?php endif; ?>
                    <?php if (isset($service['description'])): ?>
                        <td><?= $service['description']?></td>
                    <?php endif; ?>
                    <?php if (isset($service['image'])): ?>
                        <td><?= $service['image']?></td>
                    <?php endif; ?>
                        <td><a href="update-admin.php?name=<?= $service['id'] ?>" class="btn btn-secondary">Modifier</a></td>
                    <?php if (isset($service['id'])): ?>
                    <td><a href="delete-admin.php?id=<?= $service['id']?>" class="btn btn-danger">Supprimer</a></td>

                    <?php endif; ?>



            </tr>
        <?php endforeach ?>


            </tbody>
        </table>




        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

</body>
</html>