<?php
// Trouve le fichier de config pour la BDD
require_once __DIR__ . "/../src/config.php";

// Connexion à la base de donnée
$dataBaseConnection = new PDO('mysql:host=' . DB_HOST. ':3306;dbname='. DB_NAME .';charset=utf8',DB_USER,DB_PASSWORD);

$feedbacks = $dataBaseConnection
    ->query("SELECT * FROM feedbacks")
    ->fetchAll(PDO::FETCH_ASSOC);

$services = $dataBaseConnection
    ->query("SELECT * FROM services");

if ($services !== false){
    $fetchServices = $services->fetchAll(PDO::FETCH_ASSOC);
} else {
    $error = $dataBaseConnection->errorInfo();
    $errorMessage = $error[2];
}

$title = "TOUGH | Building a better world"; ?>

<html lang="fr">
    <?php include '../src/templates/head.php'?>
  <body>
    <?php include '../src/templates/menu.php' ?>
    <?php include '../src/templates/hamburger.php' ?>

    <?php include '../src/templates/header.php' ?>

    <div class="blocAbsolute"></div>
    <div class="blocAbsolute2"></div>
    <div class="globalContainer">
      <section id="section1">
        <div class="containerSection1">

          <?php include '../src/templates/kpi.php' ?>


        </div>
      </section>
      <section id="section2">
        <div class="containerServices">
          <h2>Services</h2>

          <div class="interior">
            <img id="interior" src="./img/interior.svg">
            <div class="subtitle">interior</div>
            <div class="triangle-code"></div>
          </div>
          <div class="concept">
            <img id="concept" src="./img/ideas.svg">
            <div class="subtitle">concept</div>
            <div class="triangle-code"></div>
          </div>
          <div class="residential">
            <img id="residential" src="./img/modern-house.svg">
            <div class="subtitle">residential</div>
            <div class="triangle-code"></div>
          </div>
          <div class="hospitaly">
            <img id="hospitaly" src="./img/skyline.svg">
            <div class="subtitle">hospitaly</div>
            <div class="triangle-code"></div>
          </div>
        </div>
        <div class="containerInteriorDesign">
          <div class="subContainerInteriorDesign">
            <div class="containerInterior">
              <img id="image"src="./img/interior.svg" alt="">
            </div>
            <h2>Interior</h2>
            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ut doloremque maxime quaerat sit vel dolor beatae fugit a labore reprehenderit!</p>
            <div class="buttonInterior">Learn more</div>
          </div>

        </div>
      </section>
      <section id="section3">
        <div class="emptyContainer"></div>
        <div class="containerGetStarted">
          <div class="containerHouse">
            <!-- <img id="img6" public="./img/bg_1.jpg"> -->
          </div>
          <div class="containerButton2">
            <div class="getStarted">Get started</div>
            <div class="buttonGetStarted">Request a quote</div>
          </div>

        </div>
      </section>

        <section id="section4">
            <div class="column">
                <div class="portfolio">
                    <h2>Portfolio</h2>
                    <br>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                </div>
                <a class="containerimg" href="project-single.php?project=3&data=3">
                    <div class="firstImage">
                        <div class="filterImage">
                        </div>
                    </div>
                    <div class="descriptionImage">Work 3</div>
                </a>
                <a class="containerimg" href="project-single.php?project=2&data=2">
                    <div class="classicImage1">
                        <div class="filterImage">
                        </div>
                    </div>
                    <div class="descriptionImage">Work 2</div>
                </a>
                <a class="containerimg" href="project-single.php?project=9&data=9">
                    <div class="classicImage2">
                        <div class="filterImage">
                        </div>
                    </div>
                    <div class="descriptionImage">Work 9</div>
                </a>
            </div>
            <div class="containerLargeImage">
                <a class="containerimg" href="project-single.php?project=1&data=1">
                    <div class="largeImage">
                        <div class="filterImage">
                        </div>
                    </div>
                    <div class="descriptionImage">Work 1</div>
                </a>
                <div class="containerimg">
                    <div class="containerClassic">
                        <a class="classicImage3" href="project-single.php?project=5&data=5">
                            <div class="filterImage">
                            </div>
                        </a>
                        <div class="descriptionImage">Work 5</div>
                        <div class="containerimg2"></div>

                        <a class="classicImage4" href="project-single.php?project=6&data=6">
                            <div class="filterImage">
                            </div>
                        </a>
                        <div class="descriptionImage4">Work 6</div>
                    </div>
                </div>
                <div class="containerimg">
                    <div class="containerLast">
                        <a class="mediumImage" href="project-single.php?project=7&data=7">
                            <div class="filterImage">
                            </div>
                        </a>
                        <div class="descriptionImage">Work 7</div>
                        <div class="containerimg2"></div>
                        <a class="lastImage" href="project-single.php?project=4&data=4">
                            <div class="filterImage">
                            </div>
                        </a>
                        <div class="descriptionImageLast">Work 4</div>
                    </div>
                </div>
            </div>
        </section>
      <section id=section5>
        <div class="containerClient">
          <h2>Client says</h2>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum laudantium enim facere recusandae perspic</p>
        </div>
        <div class="containerComment">
        <?php foreach ($feedbacks as $feedback): ?>
            <div class="comment1">
                <div class="containerImage">
                    <div class="clientImage1">
                        <div class="guillemet">
                        </div>
                    </div>
                </div>
                <br>
                <p><?= $feedback['textzone'] ?></p>
                <br>
                <div class="author"><?= $feedback['prenom'] ?></div>
                <br>
                <div class="profession">
                    <?php
                    if (!empty($feedback['metier'])){
                        echo $feedback['metier'] ;
                    }
                    else {
                       echo '****';

                    }
                    ?>
                </div>
            </div>
        <?php endforeach ?>
        <?php foreach ($feedbacks as $feedback): ?>
            <div class="comment2">
                <div class="containerImage">
                    <div class="clientImage2">
                        <div class="guillemet">
                        </div>
                    </div>
                </div>
                <br>
                <p><?= $feedback['textzone'] ?></p>
                <br>
                <div class="author"><?= $feedback['prenom'] ?></div>
                <br>
                <div class="profession">
                    <?php
                    if (!empty($feedback['metier'])){
                        echo $feedback['metier'] ;
                    }
                    else {
                        echo '****';

                    }
                    ?>
                </div>
            </div>
        <?php endforeach ?>
        <?php foreach ($feedbacks as $feedback): ?>
            <div class="comment3">
                <div class="containerImage">
                    <div class="clientImage3">
                        <div class="guillemet">
                        </div>
                    </div>
                </div>
                <br>
                <p><?= $feedback['textzone'] ?></p>
                <br>
                <div class="author"><?= $feedback['prenom'] ?></div>
                <br>
                <div class="profession">
                    <?php
                    if (!empty($feedback['metier'])){
                        echo $feedback['metier'] ;
                    }
                    else {
                        echo '****';

                    }
                    ?>
                </div>
            </div>
        <?php endforeach ?>


          
        </div>
        <div class="containerDot">
          <div class="dot1"></div>
          <div class="dot2"></div>
          <div class="dot3"></div>
        </div>
      </section>

    </div>
    <div class="containerSection6">
      <section id="section6">
        <div class="businessPlan">
          <div class="containerImageBusiness">
            <div class="businessImage"></div>
          </div>
          <h3>Business Plan</h3>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nulla doloum quos iusto nemo necessitatibus nihil.</p>
          <br>
          <div class="containerBottom">
            <p><span>$28 </span>per month</p>
            <div class="containerButton">
              <div class="button">
                Get Started
              </div>
            </div>
            <div class="request">Open Source</div>
          </div>
        </div>
        <div class="businessPlan">
          <div class="containerImageBusiness">
            <div class="businessImage"></div>
          </div>
          <h3>Business Plan</h3>
          <p>Lorem ipsum dolor sit a. Nulla dolor eaque laudantium fuga itaque nostrnihil.</p>
          <br>
          <div class="containerBottom">
            <p><span>$28 </span>per month</p>
            <div class="containerButton">
              <div class="button">
                Get Started
              </div>
            </div>
            <div class="request">Free 30 Day Trial</div>
          </div>
        </div>
        <div class="businessPlan">
          <div class="containerImageBusiness">
            <div class="businessImage"></div>
          </div>
          <h3>Business Plan</h3>
          <p>Lorem fuga itaque nostrum quos iusto nemo necessitatibus nihil.</p>
          <br>
          <div class="containerBottom">
            <p><span>$28 </span>per month</p>
            <div class="containerButton">
              <div class="button">
                Get Started
              </div>
            </div>
            <div class="request">Request a Quote</div>
          </div>
        </div>
      </section>
    </div>



    <?php include '../src/templates/footer.php' ?>

    <script src="./menu.js"></script>
    <script src="./main.js"></script>
  </body>
</html>
