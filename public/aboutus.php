<?php $title = "About Us" ?>

<!doctype html>
<html lang="fr">
    <?php include '../src/templates/head.php'?>

<body>
    <?php include '../src/templates/menu.php' ?>

    <?php include '../src/templates/hamburger.php' ?>

    <?php include '../src/templates/header.php' ?>

    <br>
    <br>
    <br>
    <div class="globalContainer">
        <h1 class="about">About Us</h1>

        <br>
        <br>

        <p>Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi delectus earum facere laboriosam veniam? Aspernatur consequuntur corporis cumque deleniti dicta distinctio doloremque eaque eligendi explicabo fuga fugit id illum in ipsum mollitia natus nihil nulla numquam placeat quam qui, ratione sapiente sequi temporibus unde velit, vitae voluptatibus. Autem impedit maxime, minus modi natus omnis repellendus, rerum similique sunt suscipit temporibus totam, veritatis voluptates? Beatae distinctio excepturi ipsa laboriosam molestias quod sunt ut vitae! Architecto cum dignissimos, doloribus enim facilis illum in ipsa necessitatibus nobis odio perferendis placeat praesentium quaerat quas, qui quia quidem quod quos reiciendis rem reprehenderit vel voluptas.</p>

        <br>

        <?php include "../src/templates/kpi.php" ?>

        <br>
    </div>


    <?php include '../src/templates/footer.php' ?>

    <!--    <script src="./main.js"></script>-->
    <script src="./menu.js"></script>

</body>
</html>