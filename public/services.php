<?php $title = "Services" ?>

<!doctype html>
<html lang="fr">
    <?php include '../src/templates/head.php'?>

<body>
    <?php include '../src/templates/menu.php' ?>

    <?php include '../src/templates/hamburger.php' ?>

    <br>
    <br>
    <br>
    <div class="globalContainerServices">
        <br>
        <h1>Services</h1>
        <div class="containerHospitalyServices">
            <div class="containerimgH"></div>
        </div>
        <div class="containerText">
            <h2>Hospitaly</h2>
            <br>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio esse facere hic iusto non quasi tempora tenetur! Ad cumque earum in nemo neque, quam repellendus? Accusamus accusantium aliquid aut autem deleniti dicta dolore dolores dolorum eius eos eveniet ex facere illum libero magnam magni molestias mollitia natus nihil, non pariatur quae quidem quisquam, ratione rem sunt velit voluptas? Accusantium culpa debitis dignissimos dolore ea excepturi facere ipsam laboriosam, maxime pariatur porro, quos sint sunt tempora temporibus voluptas, voluptatibus! Ab aliquam amet commodi doloremque dolores dolorum, ducimus est explicabo fuga ipsam nihil odio officiis praesentium qui reprehenderit sunt tempore velit veritatis?</p>
        </div>
        <div class="containerText2">
            <h2>Interior</h2>
            <br>

            <p>Lorem ipsum dolor sio neque, quam repellendus? Accusamus accusantium aliquid aut autem deleniti dicta dolore dolores dolorum eius eos eveniet ex facere illum libero magnam magni molestias mollitia natus nihil, non pariatur quae quidem quisquam, ratione rem sunt velit voluptas? Accusantium culpa debitis dignissimos dolore ea excepturi facere ipsam laboriosam, maxime pariatur porro, quos sint sunt tempora temporibus voluptas, voluptatibus! Ab aliquam amet commodi doloremque dolores dolorum, ducimus est explicabo fuga ipsam nihil odio officiis praesentium qui reprehenderit sunt tempore velit veritatis?</p>
        </div>
        <div class="containerInteriorServices">
            <div class="containerimgI"></div>
        </div>
        <div class="containerHospitalyServices">
            <div class="containerimgR"></div>
        </div>
        <div class="containerText">
            <h2>Residential</h2>
            <br>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio esse facere hic iusto non quasi tempora tenetur! Ad cumque earum in nemo neque, quam repellendus? Accusamus accusantium aliquid aut autem deleniti dicta dolore dolores dolorum eius eos eveniet ex facere illum libero magnam magni molestias mollitia natus nihil, non pariatur quae quidem quisquam, ratione rem sunt velit voluptas? Accusantium culpa debitis dignissimos dolore ea excepturi facere ipsam laboriosam, maxime pariatur porro, quos sint sunt tempora temporibus voluptas, voluptatibus! Ab aliquam amet commodi doloremque dolores dolorum, ducimus est explicabo fuga ipsam nihil odio officiis praesentium qui reprehenderit sunt tempore velit veritatis?</p>
        </div>
        <div class="containerText2">
            <h2>Concept</h2>
            <br>

            <p>Lorem ipsum dolor sio neque, quam repellendus? Accusamus accusantium aliquid aut autem deleniti dicta dolore dolores dolorum eius eos eveniet ex facere illum libero magnam magni molestias mollitia natus nihil, non pariatur quae quidem quisquam, ratione rem sunt velit voluptas? Accusantium culpa debitis dignissimos dolore ea excepturi facere ipsam laboriosam, maxime pariatur porro, quos sint sunt tempora temporibus voluptas, voluptatibus! Ab aliquam amet commodi doloremque dolores dolorum, ducimus est explicabo fuga ipsam nihil odio officiis praesentium qui reprehenderit sunt tempore velit veritatis?</p>
        </div>
        <div class="containerInteriorServices">
            <div class="containerimgC"></div>
        </div>
    </div>

    <?php include '../src/templates/footer.php' ?>

    <!--    <script src="./main.js"></script>-->
    <script src="./menu.js"></script>

</body>
</html>