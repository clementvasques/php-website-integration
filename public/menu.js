let hamburger = document.querySelector(".hamburger")
let menu = document.querySelector(".menu")
let firstLine = document.querySelector(".firstLine")
let secondLine = document.querySelector(".secondLine")
let thirdLine = document.querySelector(".thirdLine")

let containerHamburger = document.querySelector(".containerHamburger")

let clicCount = 0
containerHamburger.addEventListener('click', function(){
    firstLine.style.background = "white"
    secondLine.style.background = "white"
    thirdLine.style.background = "white"

    clicCount++
    menu.style.display = "flex"

    firstLine.style.transform = "rotate(45deg)"
    firstLine.style.transition = "0.7s"
    secondLine.style.transform = "rotate(-45deg)"
    secondLine.style.position = "absolute"
    secondLine.style.top = "0"

    firstLine.style.boxShadow = "none"
    secondLine.style.boxShadow = "none"


    thirdLine.style.display="none"
    if (clicCount%2===0){
        menu.style.animationName = "menuOff"
        menu.style.display = "none"

        firstLine.style.transform = "rotate(0deg)"
        firstLine.style.transition = "0.7s"

        secondLine.style.transform = "rotate(0deg)"
        secondLine.style.transition = "0.7s"

        thirdLine.style.display="block"
        secondLine.style.top = "15px"

        firstLine.style.background = "rgb(52, 44, 73)"
        secondLine.style.background = "rgb(52, 44, 73)"
        thirdLine.style.background = "rgb(52, 44, 73)"
    } else {
        menu.style.animationName = "menu"

    }
})


