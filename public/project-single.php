<?php
    $totalProjects = 104;

    $nbrProjectsPage = 10;

    $totalPages = $totalProjects/$nbrProjectsPage + 1;

    if (!empty($_GET['project'])) {
    // Le paramètre est bien passé, on doit regarder s'il est valide

    // Tout ce qui vient de $_GET est de type "string", donc on convertit en "integer"
    $projectNumber = intval($_GET['project']);

    }
    if (!empty($_GET['page'])){
        $getCurrentPage = intval($_GET['page']);
    }




// Si le numéro de projet est invalide, on gère cette erreur
    if (empty($projectNumber) || $projectNumber <= 0) {
        // on peut décider d'afficher le projet 1 par défaut
        $projectNumber = 1;
    }
    if (empty($getCurrentPage) || $getCurrentPage <= 0){
        $getCurrentPage = 1;
    }

    if ($getCurrentPage>$totalPages){
        $getCurrentPage = floor($totalPages);
    }


    // ou bien de faire une erreur (404 par exemple)
    // throw new Exception("Projet manquant");

    // ou bien le rediriger sur une autre page
?>

<?php $title = "Project-single "?>



<!doctype html>
<html lang="fr">
<?php include '../src/templates/head.php'?>

<body>
<?php include '../src/templates/menu.php' ?>

<?php include '../src/templates/hamburger.php' ?>

<div class="globalContainer">
    <br>
    <br>
    <br>

    <div class="containerimgSingle">

        <?php
        $projectNumber=0;

        $end = $getCurrentPage*$nbrProjectsPage;
        $start = $end - $nbrProjectsPage;


        for ($work = $start + 1; $work<$end + 1 ; $work++){
            $projectNumber++;

            if ($projectNumber>9)
            {
                $projectNumber = 1;
            }

            echo '<div class="containerArticle">';
            echo '<div class="singleImage" style="background-image: url(./img/image_'.$projectNumber.'.jpg)">'.'</div>';
            echo '<div class="containerSecondBloc">';
                echo '<div class="titleImage">#Work '.$work.' </div>';
                echo '<p class="paragrapheArticle">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus blanditiis culpa distinctio dolor dolorum eligendi eveniet hic id labore nostrum officiis, omnis perspiciatis quibusdam saepe sunt voluptate voluptatibus! Fuga, sequi, vitae. Distinctio in ipsum obcaecati omnis veritatis. Ad aliquid aspernatur dolorem illum maiores molestias mollitia odio officiis sunt veritatis. Tempore. Sequi veniam vitae. Commodi error est exercitationem incidunt ipsum maxime praesentium qui</p>';
            echo '</div>';
            echo '</div>';

            if ($work >= $totalProjects){
                break;
            }
        }

        ?>

    </div>
    <div class="containerPagination">
        <a href="project-single.php?project=<?= $projectNumber - 1 ?>&page=<?= $getCurrentPage - 1?>" class="precedent"><</a>

        <?php
        $currentPage = 0;
        for ($i = 1; $i<=$totalPages; $i++){
            $currentPage++;
            echo '<a class="pagination" href="project-single.php?page=' .$currentPage.'" >' .$i. '</a>';
        }
        ?>
        <a href="project-single.php?project=<?= $projectNumber + 1 ?>&page=<?= $getCurrentPage + 1?>" class="suivant">></a>

<!--        if ($getCurrentPage <= $totalPages){-->
<!--        echo '<a class="suivant" href="project-single.php?project=' .($projectNumber + 1). '&page=' .($getCurrentPage + 1).'">></a>';-->
<!--        }-->
    </div>

</div>


<?php include '../src/templates/footer.php' ?>


<script src="./menu.js"></script>

</body>
</html>

